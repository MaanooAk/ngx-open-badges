import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdvancedFormItemResolver } from '@universis/forms';
import { BadgeClassEditComponent } from './components/badge-classes/edit.component';
import { BadgeClassListComponent } from './components/badge-classes/list.component';
import { BadgeProfileListComponent } from './components/profiles/list.component';
import { BadgeProfileEditComponent } from './components/profiles/edit.component';

const routes: Routes = [
  {
    'path': '',
    'pathMatch': 'full',
    'redirectTo': 'items'
  },
  {
    path: 'items/:id',
    component: BadgeClassEditComponent
  },
  {
    path: 'items',
    component: BadgeClassListComponent
  },
  {
    path: 'profiles/:id',
    component: BadgeProfileEditComponent
  },
  {
    path: 'profiles',
    component: BadgeProfileListComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class OpenBadgesRoutingModule { }
