import { CUSTOM_ELEMENTS_SCHEMA, NgModule, OnInit, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AdvancedFormsModule } from '@universis/forms';
import { FormsModule } from '@angular/forms';
import { MostModule } from '@themost/angular';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { NgArrayPipesModule } from 'ngx-pipes';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { SharedModule } from '@universis/common';
import { TablesModule } from '@universis/ngx-tables';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { BadgeClassListComponent } from './components/badge-classes/list.component';
import { OpenBadgesRoutingModule } from './admin-routing.module';
import { BadgeClassEditComponent } from './components/badge-classes/edit.component';
import { BadgeProfileListComponent } from './components/profiles/list.component';
import { BadgeProfileEditComponent } from './components/profiles/edit.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    OpenBadgesRoutingModule,
    AdvancedFormsModule,
    MostModule,
    SharedModule,
    TranslateModule,
    NgArrayPipesModule,
    NgxDropzoneModule,
    TablesModule,
    NgxDropzoneModule,
    NgxExtendedPdfViewerModule
  ],
  declarations: [
    BadgeClassListComponent,
    BadgeClassEditComponent,
    BadgeProfileListComponent,
    BadgeProfileEditComponent
  ],
  exports: [
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class OpenBadgesAdminModule {
}
