import { TableConfiguration } from "@universis/ngx-tables";

// tslint:disable: quotemark
export const OpenBadgeClassesListConfiguration: TableConfiguration = {
  "title": "BadgeClasses.Title",
  "model": "OpenBadgeClasses",
  // tslint:disable-next-line: max-line-length
  "searchExpression": "indexof(name, '${text}') ge 0 or indexof(issuer/name, '${text}') ge 0",
  "selectable": true,
  "multipleSelect": true,
  "columns": [
    {
      "name": "_id",
      "title": "ID",
      "hidden": true
    },
    {
      "name": "id",
      "title": "",
      "formatter": "ButtonFormatter",
      "formatOptions": {
        "buttonContent": "<i class=\"far fa-eye text-indigo\"></i>",
        "buttonClass": "btn btn-default",
        "commands": [ "${_id}" ],
        "navigationExtras": {
        }
      }
    },
    {
      "name": "name",
      "property": "name",
      "title": "Name"
    },
    {
      "name": "issuer/name",
      "property": "issuerName",
      "title": "Issuer"
    },
    {
      "name": "image/id",
      "property": "image",
      "title": "Image"
    }
  ],
  "defaults": {
  },
  "criteria": [
    {
      "name": "name",
      "filter": "(indexof(name, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "description",
      "filter": "(description eq '${value}')",
      "type": "text"
    },
    {
      "name": "issuerName",
      "filter": "(issuer/name eq '${value}')",
      "type": "text"
    },
    {
      "name": "issuer",
      "filter": "issuer eq '${value}'",
      "type": "text"
    },
    {
      "name": "tag",
      "filter": "(indexof(tags/tag, '${value}') ge 0)",
      "type": "text"
    }
  ],
  "searches": []
};
