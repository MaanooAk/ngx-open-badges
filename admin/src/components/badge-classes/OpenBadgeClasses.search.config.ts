export const OpenBadgeClassSearchConfiguration = {
  "components": [
    {
      "label": "Columns",
      "columns": [
        {
          "components": [
            {
              "label": "Issuer",
              "widget": "choicesjs",
              "dataSrc": "url",
              "data": {
                "url": "OpenBadgeProfiles",
                "headers": []
              },
              "selectThreshold": 0.3,
              "validate": {
                "unique": false,
                "multiple": false
              },
              "template": "{{ item.name }}",
              "key": "issuer",
              "valueProperty": "id",
              "selectValues": "value",
              "type": "select",
              "input": true,
              "hideOnChildrenHidden": false,
              "disableLimit": false,
              "lazyLoad": false
            },
            {
              "label": "Tag",
              "spellcheck": true,
              "tableView": true,
              "validate": {
                "unique": false,
                "multiple": false
              },
              "key": "tag",
              "type": "textfield",
              "input": true,
              "hideOnChildrenHidden": false
            }
          ],
          "width": 6,
          "offset": 0,
          "push": 0,
          "pull": 0
        },
        {
          "components": [
            {
              "label": "Name",
              "spellcheck": true,
              "tableView": true,
              "validate": {
                "unique": false,
                "multiple": false
              },
              "key": "name",
              "type": "textfield",
              "input": true,
              "hideOnChildrenHidden": false
            },
            {
              "label": "Description",
              "spellcheck": true,
              "tableView": true,
              "validate": {
                "unique": false,
                "multiple": false
              },
              "key": "description",
              "type": "textfield",
              "input": true,
              "hideOnChildrenHidden": false
            }
          ],
          "width": 6,
          "offset": 0,
          "push": 0,
          "pull": 0
        }
      ],
      "tableView": false,
      "key": "department",
      "type": "columns",
      "input": false,
      "path": "columns"
    }
  ]
}
