import { Component, OnDestroy, OnInit, ViewEncapsulation } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AngularDataContext } from "@themost/angular";

@Component({
    selector: 'universis-badge-classes-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss'],
    encapsulation: ViewEncapsulation.None
  })
  export class BadgeClassEditComponent implements OnInit, OnDestroy {
    subscription: any;
    model: any;

    constructor(private activatedRoute: ActivatedRoute,
        private context: AngularDataContext,
        private router: Router) {

    }

    ngOnDestroy(): void {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }
    ngOnInit(): void {
        this.subscription = this.activatedRoute.params.subscribe((params) => {
            this.context.model('OpenBadgeClasses').where('_id').equal(params.id)
                .expand('criteria', 'tags')
                .getItem().then((item: any) => {
                this.model = item;
                console.log(this.model);
            });
        });
    }

    save() {
        this.context.model('OpenBadgeClasses').save(this.model).then(() => {
            return this.router.navigate(['/badges', 'items'])
        });
    }

  }
