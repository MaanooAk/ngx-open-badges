import { TableConfiguration } from "@universis/ngx-tables";

// tslint:disable: quotemark
export const OpenBadgeProfilesListConfiguration: TableConfiguration = {
  "title": "Profiles.Title",
  "model": "OpenBadgeProfiles",
  // tslint:disable-next-line: max-line-length
  "searchExpression": "indexof(name, '${text}') ge 0",
  "selectable": true,
  "multipleSelect": true,
  "columns": [
    {
      "name": "_id",
      "title": "ID",
      "hidden": true
    },
    {
      "name": "id",
      "title": "",
      "formatter": "ButtonFormatter",
      "formatOptions": {
        "buttonContent": "<i class=\"far fa-eye text-indigo\"></i>",
        "buttonClass": "btn btn-default",
        "commands": [ "${_id}" ],
        "navigationExtras": {
        }
      }
    },
    {
      "name": "name",
      "property": "name",
      "title": "Name"
    },
    {
      "name": "url",
      "property": "url",
      "title": "URL"
    },
    {
      "name": "telephone",
      "property": "telephone",
      "title": "Tel."
    },
    {
      "name": "description",
      "property": "description",
      "title": "Description"
    },
    {
      "name": "email",
      "property": "email",
      "title": "Email"
    }
  ],
  "defaults": {
  },
  "criteria": [
    {
      "name": "name",
      "filter": "(indexof(name, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "url",
      "filter": "(url eq '${value}')",
      "type": "text"
    },
    {
      "name": "telephone",
      "filter": "(telephone eq '${value}')",
      "type": "text"
    },
    {
      "name": "description",
      "filter": "description eq '${value}'",
      "type": "text"
    },
    {
      "name": "email",
      "filter": "(email eq '${value}')",
      "type": "text"
    }
  ],
  "searches": []
};
