import { Component, OnDestroy, OnInit, ViewEncapsulation } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AngularDataContext } from "@themost/angular";

@Component({
    selector: 'universis-profiles-edit',
    templateUrl: './edit.component.html',
  })
  export class BadgeProfileEditComponent implements OnInit, OnDestroy {
    subscription: any;
    model: any;

    constructor(private activatedRoute: ActivatedRoute,
        private context: AngularDataContext,
        private router: Router) {

    }

    ngOnDestroy(): void {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }
    ngOnInit(): void {
        this.subscription = this.activatedRoute.params.subscribe((params) => {
            this.context.model('OpenBadgeProfiles').where('_id').equal(params.id)
                .getItem().then((item: any) => {
                this.model = item;
            });
        });
    }

    save() {
        this.context.model('OpenBadgeProfiles').save(this.model).then(() => {
            return this.router.navigate(['/badges', 'profiles'])
        });
    }

  }
