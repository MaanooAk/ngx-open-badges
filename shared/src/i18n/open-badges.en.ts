/* tslint:disable max-line-length */
// tslint:disable: quotemark
export const en = {
  "OpenBadges": {
    "Badge": "Badge",
    "Badges": "Badges",
    "BadgeClasses": {
      "Title": "Badge Classes",
      "Description": "A collection of information about the accomplishment recognized by the Open Badge. Many assertions may be created corresponding to one badge class.",
      "Name": "Name",
      "DescriptionForm": "Description",
      "Image": "Image",
      "Issuer": "Issuer",
      "Tags": "Tags",
      "Criteria": "Criteria"

    },
    "Profiles": {
      "Title": "Profiles",
      "Description": "A Profile is a collection of information that describes the entity or organization using Open Badges. Issuers must be represented as Profiles, and recipients, endorsers, or other entities may also be represented using this vocabulary. Each Profile that represents an Issuer may be referenced in many BadgeClasses that it has defined. Anyone can create and host an Issuer file to start issuing Open Badges. Issuers may also serve as recipients of Open Badges, often identified within an Assertion by specific properties, like their url or contact email address. An Issuer Profile is a subclass of the general Profile with some additional requirements.",
    }
  },
  "Sidebar": {
    "Badges": "Badges"
  },
};
